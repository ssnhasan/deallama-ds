package com.is2.deallama.deallamads;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DeallamaDsApplication {

	public static void main(String[] args) {
		SpringApplication.run(DeallamaDsApplication.class, args);
	}

}
