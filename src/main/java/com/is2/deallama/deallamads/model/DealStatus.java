package com.is2.deallama.deallamads.model;

public enum DealStatus {
    ACTIVE,
    EXPIRED
}
