package com.is2.deallama.deallamads.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.AllArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity @Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class User {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;
    private String firstName;
    private String lastName;
    @CreationTimestamp
    private Timestamp registerTime;
    @Enumerated(EnumType.STRING)
    private UserStatus status = UserStatus.LOCKED;
    private int numPosts;
    private String userName;
    private String email;
    private String role;
}
