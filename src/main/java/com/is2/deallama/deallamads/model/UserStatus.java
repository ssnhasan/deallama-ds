package com.is2.deallama.deallamads.model;

public enum UserStatus {
    ACTIVE("active"),
    INACTIVE("inactive"),
    LOCKED("locked"),
    BANNED("banned");

    private String status;

    private UserStatus(String status) {
        this.status=status;
    }

    public boolean isValidUserStatus(String input) {
        for (UserStatus us:UserStatus.values()) {
            if (us.getStatus().equalsIgnoreCase(input)){
                return true;
            }
        }
        return false;
    }

    public String getStatus() {
        return this.status;
    }
}
