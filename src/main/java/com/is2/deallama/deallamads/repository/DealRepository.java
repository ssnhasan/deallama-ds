package com.is2.deallama.deallamads.repository;

import com.is2.deallama.deallamads.model.Deal;
import org.springframework.data.repository.CrudRepository;

public interface DealRepository extends CrudRepository<Deal, Integer> {
}
