package com.is2.deallama.deallamads.repository;

import com.is2.deallama.deallamads.model.Category;
import com.is2.deallama.deallamads.model.User;
import org.springframework.data.repository.CrudRepository;

public interface CategoryRepository extends CrudRepository<Category, Integer> {
}
