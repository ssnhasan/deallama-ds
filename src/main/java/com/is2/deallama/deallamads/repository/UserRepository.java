package com.is2.deallama.deallamads.repository;

import com.is2.deallama.deallamads.model.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Integer> {
}
