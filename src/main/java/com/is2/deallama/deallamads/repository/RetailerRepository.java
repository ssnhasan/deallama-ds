package com.is2.deallama.deallamads.repository;

import com.is2.deallama.deallamads.model.Retailer;
import com.is2.deallama.deallamads.model.User;
import org.springframework.data.repository.CrudRepository;

public interface RetailerRepository extends CrudRepository<Retailer, Integer> {
}
