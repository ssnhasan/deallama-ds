package com.is2.deallama.deallamads.controller;

import com.is2.deallama.deallamads.model.Category;
import com.is2.deallama.deallamads.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path="/category")
public class CategoryController {
    @Autowired
    private CategoryRepository categoryRepository;

    @PostMapping(path="/add")
    public @ResponseBody
    String addNewCategory (
            @RequestParam String name,
            @RequestParam String description) {
        Category c = new Category();
        c.setName(name);
        c.setDescription(description);
        categoryRepository.save(c);
        return "Saved " + c.getId();
    }

    @GetMapping(path="/all")
    public @ResponseBody Iterable<Category> getAllCategories() {
        return categoryRepository.findAll();
    }
}
