package com.is2.deallama.deallamads.controller;

import com.is2.deallama.deallamads.model.Retailer;
import com.is2.deallama.deallamads.repository.RetailerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path="/retailer")
public class RetailerController {
    @Autowired
    private RetailerRepository retailerRepository;

    @PostMapping(path="/add")
    public @ResponseBody
    String addNewRetailer (
            @RequestParam String name,
            @RequestParam String description,
            @RequestParam String address) {
        Retailer r = new Retailer();
        r.setName(name);
        r.setDescription(description);
        r.setAddress(address);
        retailerRepository.save(r);
        return "Saved " + r.getId();
    }

    @GetMapping(path="/all")
    public @ResponseBody Iterable<Retailer> getAllRetailer() {
        return retailerRepository.findAll();
    }
}
