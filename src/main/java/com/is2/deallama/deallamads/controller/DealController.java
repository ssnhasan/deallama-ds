package com.is2.deallama.deallamads.controller;

import com.is2.deallama.deallamads.model.Category;
import com.is2.deallama.deallamads.model.Deal;
import com.is2.deallama.deallamads.model.Retailer;
import com.is2.deallama.deallamads.model.User;
import com.is2.deallama.deallamads.repository.CategoryRepository;
import com.is2.deallama.deallamads.repository.DealRepository;
import com.is2.deallama.deallamads.repository.RetailerRepository;
import com.is2.deallama.deallamads.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping(path="/deal")
public class DealController {
    @Autowired
    private DealRepository dealRepository;
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private RetailerRepository retailerRepository;
    @Autowired
    private UserRepository userRepository;

    @PostMapping(path="/add")
    public @ResponseBody
    String addNewDeal (
            @RequestParam int categoryId,
            @RequestParam int retailerId,
            @RequestParam int userId,
            @RequestParam String description,
            @RequestParam int numViews) throws Exception {
        Deal d = new Deal();
        //go to the repos to grab the values for category/retailer/user based on the given ids
        Optional<Category> opCategory =  categoryRepository.findById(categoryId);
        Optional<Retailer> opRetailer =  retailerRepository.findById(retailerId);
        Optional<User> opUser =  userRepository.findById(userId);

        //if we got all three we can set them accordingly. If not, should we throw an exception? Probably.
        opCategory.ifPresent(category -> {
            opRetailer.ifPresent(retailer -> {
                opUser.ifPresent(user -> {
                    d.setCategory(category);
                    d.setRetailer(retailer);
                    d.setUser(user);
                    d.setDescription(description);
                    d.setNumViews(numViews);

                });
            });
        });
        dealRepository.save(d);
        return "Saved...maybe?";
    }

    @GetMapping(path="/all")
    public @ResponseBody Iterable<Deal> getAllDeals() {
        return dealRepository.findAll();
    }
}
