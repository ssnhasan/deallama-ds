Before attempting to start this, please ensure that the database is running and working properly (eg use SQL Workbench or some other method to test that deallama_db exists and whatnot).

Note also that there are some simple postman scripts in the resources/static folder

Once ready, use ./gradlew bootRun to start this system up, and use 127.0.0.1:8080 to verify that you hit the helloController
